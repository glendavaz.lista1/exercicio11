public class Aluno
{
    // instance variables - replace the example below with your own
    private String nome;
    private int numeroCandidato;
    private int numeroVotos = 0;
    
    
    public void setNome(String paramNome) {
        this.nome = paramNome;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNumeroCandidato(int paramNumeroCandidato) {
        this.numeroCandidato = paramNumeroCandidato;
    }
    
    public int getNumeroCandidato() {
        return this.numeroCandidato;
    }
    
    public void setNumeroVotos() {
        this.numeroVotos = 1 + this.numeroVotos;
    }
    
    public int getNumeroVotos() {
        return this.numeroVotos;
    }
    
}
