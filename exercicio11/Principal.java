import java.util.Scanner;
public class Principal
{
    public static void main (String[] args) {
        Scanner le = new Scanner(System.in);
        
        Eleicao eleicao = new Eleicao();
        
        while(true) {
            System.out.println("### Cadastro de Alunos");
            Aluno aluno = new Aluno();
            System.out.println("Insira o nome do aluno:");
            aluno.setNome(le.next());
            System.out.println("Insira o numero de inscricao do aluno:");
            aluno.setNumeroCandidato(le.nextInt());   
            eleicao.addAluno(aluno);            
            System.out.println("Deseja cadastrar outro aluno? (S ou N)");
            if(le.next().equalsIgnoreCase("n")) {
                break;
            }            
        }
        
        for(int i = 0; i < eleicao.getAlunoList().size(); i++) {
            System.out.println("### Votacao");
            System.out.println("Insira o numero do candidato que deseja votar:");
            eleicao.votar(le.nextInt());            
        }
        
        System.out.println("O Aluno ganhador e: " + eleicao.calculaGanhador().getNome() + " e o percentual de votacao foi " + eleicao.calculaPercentualGanhador(eleicao.calculaGanhador()) + "%" );
        
            
    }
  
}
