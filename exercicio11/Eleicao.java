import java.util.ArrayList;

public class Eleicao
{
    private Aluno[] aluno;
    private ArrayList<Aluno> alunoList;
    private Aluno alunoGanhador;

    public Eleicao()
    {
      aluno = new Aluno[10];
      alunoList = new ArrayList<>();
    }
    
    public ArrayList<Aluno> getAlunoList() {
        return alunoList;
    }
    
    public void addAluno(Aluno aluno) {
        this.alunoList.add(aluno);
    }
    
    public void votar(int paramNumeroCandidato) {
        for(int i = 0; i < alunoList.size(); i++) {
              if(alunoList.get(i).getNumeroCandidato() == paramNumeroCandidato) {
                  alunoList.get(i).setNumeroVotos();
              }
        }   
    }
    
    public Aluno calculaGanhador() {
        int numeroVotosCandidatoMaisVotado = 0;
        int numeroCandidato = 0;
        
        for(int i = 0; i < alunoList.size(); i++) {
           if(alunoList.get(i).getNumeroVotos() > numeroVotosCandidatoMaisVotado) {
               this.alunoGanhador = alunoList.get(i);
           }
        }        
      
      return this.alunoGanhador;
    }
    
    public double calculaPercentualGanhador(Aluno aluno) {
                
        int totalVotos = alunoList.size();
        double percentual = (aluno.getNumeroVotos() / totalVotos) * 100;   
      
      return percentual;
    }
    
}
